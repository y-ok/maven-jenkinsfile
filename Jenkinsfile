pipeline {
   agent {
       node {
           label 'devHost'
       }
   }
   environment {
       DB_SID = 'ORCLCDB'
       gitUrl = 'https://gitlab.com/y-ok/flyway-sandbox.git'
       javaDir = 'src/main/java'
       javadocDir = 'target/site/apidocs'
       jacocoReportDir = 'target/site/jacoco'
       siteReportDir = 'target/site'
   }
   parameters {
       gitParameter(branchFilter: 'origin/(.*)', defaultValue: 'master', name: 'BRANCH_NAME', type: 'PT_BRANCH', useRepository: '.*flyway-sandbox.git', description: 'ブランチ名を指定')
   }
   options {
       ansiColor('xterm')
       buildDiscarder(
           logRotator(
               // number of build logs to keep
               numToKeepStr:'15',
               // history to keep in days
               daysToKeepStr: '15',
               // artifacts are kept for days
               artifactDaysToKeepStr: '15',
               // number of builds have their artifacts kept
               artifactNumToKeepStr: '15'
            )
        )
   }
   stages {
      stage('SetUp') {
         steps {
             cleanWs()
             // DBインスタンス再起動
             script {
                 sh "docker restart ${DB_SID}"
                 sleep(15)
             }
             git branch: "${params.BRANCH_NAME}", credentialsId: '375df0e7-0ad6-48cd-8ec6-8db21979617e', url: "${gitUrl}"
             script {
                 if ("${params.BRANCH_NAME}".contains("master")) {
                     manager.addShortText("${params.BRANCH_NAME}", "White", "RoyalBlue", "1px", "RoyalBlue")
                 } else {
                     manager.addShortText("${params.BRANCH_NAME}", "White", "MediumSeaGreen", "1px", "MediumSeaGreen")
                 }
             }
             withMaven(maven: 'maven3', options: [jacocoPublisher(disabled: true), findbugsPublisher(disabled: true)]) {
                 sh "mvn package -Dmaven.test.skip=true -Djacoco.skip=true"
                 sh "mvn flyway:clean"
                 sh "mvn flyway:migrate"
             }
         }
      }
      stage('UnitTest') {
         steps {
            withMaven(maven: 'maven3') {
               sh "mvn clean test"
               sh "mvn jacoco:report"
            }
         }
      }
      stage('Static Analysis') {
         steps {
             parallel(
                 'checkstyle': {
                     withMaven(maven: 'maven3') {
                         sh "mvn checkstyle:check"
                     }
                 },
                 'Spotbugs': {
                     withMaven(maven: 'maven3') {
                         sh "mvn spotbugs:spotbugs"
                     }
                 },
                 'Javadoc': {
                     withMaven(maven: 'maven3', , options: [jacocoPublisher(disabled: true), findbugsPublisher(disabled: true)]) {
                         sh "mvn javadoc:javadoc"
                         step([
                             $class: 'JavadocArchiver',
                             javadocDir: "${javadocDir}",
                             keepAll: true])
                     }
                 },
                 'Site': {
                     withMaven(maven: 'maven3', options: [jacocoPublisher(disabled: true), findbugsPublisher(disabled: true)]) {
                         sh "mvn site"
                         publishHTML(target: [reportName: 'Siteレポート', reportDir: "${siteReportDir}", reportFiles: 'index.html', keepAll: false])
                     }
                 })
         }
      }
   }
   post {
       always {
           recordIssues tool: checkStyle()
           recordIssues tool: spotBugs()
           archiveArtifacts artifacts: "${jacocoReportDir}/**/*.*", fingerprint: true
       }
   }
}